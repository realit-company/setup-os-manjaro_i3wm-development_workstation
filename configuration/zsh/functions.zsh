# Compressed file expander
# (from https://github.com/myfreeweb/szhuery/blob/master/zshuery.sh)
ex() {
    if [[ -f $1 ]]; then
        case $1 in
            *.tar.bz2|*.tbz|*.tbz2) tar xvfj $1;;
            *.tar.gz|*.tgz) tar xvzf $1;;
            *.tar.xz|*.txz) tar xvJf $1;;
            *.tar.lzma) tar --lzma xvf $1;;
            *.bz2) bunzip $1;;
            *.gz) gunzip $1;;
            *.rar) unrar x $1;;
            *.tar) tar xvf $1;;
            *.zip|*.war|*.jar|*.aar) unzip $1;;
            *.Z) uncompress $1;;
            *.7z) 7z x $1;;
            *) echo "'$1' cannot be extracted via ex";;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

# Any function from http://onethingwell.org/post/14669173541/any
# Search for running processes
any() {
    emulate -L zsh
    unsetopt KSH_ARRAYS
    if [[ -z "$1" ]]; then
        echo "any - grep process(es) by keyword" >&2
        echo "Usage: any keyword" >&2
        return 1
    else
        ps xauwww | /bin/grep -i --color=auto "[${1[1]}]${1[2,-1]}"
    fi
}
