#########################################################################################
#  ____  ____   __   __        __  ____    __  ____     ___  __   __ _  ____  __  ___   #
# (  _ \(  __) / _\ (  )   ___(  )(_  _)  (  )( __ \   / __)/  \ (  ( \(  __)(  )/ __)  #
#  )   / ) _) /    \/ (_/\(___))(   )(     )(  (__ (  ( (__(  O )/    / ) _)  )(( (_ \  #
# (__\_)(____)\_/\_/\____/    (__) (__)   (__)(____/   \___)\__/ \_)__)(__)  (__)\___/  #
#                                                                                       #
#########################################################################################


########################################
#                                      #
#                ACTION                #
#                                      #
########################################

# mod key definition
set $mod Mod4

# restart i3
bindsym $mod+Shift+r restart

########################################
#              terminal                #
########################################

# show terminal
bindsym $mod+Return exec termite


########################################
#           screen brightness          #
########################################

# xbacklight - increases screen brightness of 5%
#bindsym XF86MonBrightnessUp exec xbacklight -inc 5
bindsym XF86MonBrightnessUp exec light -A 5

# xbacklight - decreases screen brightness of 5%
#bindsym XF86MonBrightnessDown exec xbacklight -dec 5
bindsym XF86MonBrightnessDown exec light -U 5


########################################
#              screenshot              #
########################################

# flameshot - region screenshot
bindsym Print exec --no-startup-id flameshot gui


########################################
#            user interfaces           #
########################################

# bmenu (system administration settings cli)
bindsym $mod+b exec termite -e 'bmenu'


########################################
#                                      #
#           WINDOWS MANAGEMENT         #
#                                      #
########################################

########################################
#     current window focus change      #
########################################

# set focus to the top window
bindsym $mod+Up focus up

# set focus to the right window
bindsym $mod+Right focus right

# set focus to the bottom window
bindsym $mod+Down focus down

# set focus to the left window
bindsym $mod+Left focus left


########################################
#         move focused window          #
########################################

# move the focused window to the top
bindsym $mod+Shift+Up move up

# move the focused window to the right
bindsym $mod+Shift+Right move right

# move the focused window to the bottom
bindsym $mod+Shift+Down move down

# move the focused window to the left
bindsym $mod+Shift+Left move left


########################################
#           action on window           #
########################################

# kill focused window
bindsym $mod+Shift+q kill

# start xkill
bindsym Ctrl+$mod+x --release exec --no-startup-id xkill
# move floating windows with the mouse cursor

# floating modifier action key definition
floating_modifier $mod

# set focused window fullscreen
bindsym $mod+f fullscreen toggle

# switch focused window between tiling or floating display
bindsym $mod+Shift+space floating toggle

# resize focused floating window
mode "resize" {
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}
bindsym $mod+r mode "resize"


########################################
#           split orientation          #
########################################

#split horizontally
bindsym $mod+h split h;exec notify-send 'tile horizontally'

#split vertically
bindsym $mod+v split v;exec notify-send 'tile vertically'


########################################
#                                      #
#           POWER MANAGEMENT           #
#                                      #
########################################

########################################
#             acpi management          #
########################################

bindsym $mod+Shift+p exec "$HOME/.scripts/anytime/rofi/menu_power &"
bindsym XF86PowerOff exec "$HOME/.scripts/anytime/rofi/menu_power &"


########################################
#                                      #
#         APPLICATION SHORTCUT         #
#                                      #
########################################

########################################
#            F function keys           #
########################################

# F1 - markdown editor
bindsym $mod+F1 exec typora

# F1+Shift - integrated development environment
bindsym $mod+Shift+F1 exec vscodium

# F2 - web browser default
bindsym $mod+F2 exec google-chrome-stable

# F2 - web browser development
bindsym $mod+Shift+F2 exec firefox-developer-edition

# F3 - file browser
bindsym $mod+F3 exec termite -e 'ranger'

# F3+Shift - file browser as root
bindsym $mod+Shift+F3 exec termite -e 'sudo ranger'


########################################
#                others                #
########################################

# rofi - application launcher
bindsym $mod+d exec $HOME/.scripts/anytime/rofi/menu_run

# rofi - settings launcher
bindsym $mod+Shift+d exec $HOME/.scripts/anytime/rofi/menu_settings

# rofi - meteo 
bindsym $mod+Shift+m exec $HOME/.scripts/anytime/rofi/menu_meteo


########################################
#                                      #
#         WORKSPACE MANAGEMENT         #
#                                      #
########################################

########################################
#            workspace names           #
########################################

set $ws1 1
set $ws2 2
set $ws3 3
set $ws4 4
set $ws5 5
set $ws6 6
set $ws7 7
set $ws8 8
set $ws9 9
set $ws10 10


########################################
#        switch to a workspace         #
########################################

bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10


########################################
#  move focused window to a workspace  #
########################################

bindsym $mod+Ctrl+1 move container to workspace $ws1
bindsym $mod+Ctrl+2 move container to workspace $ws2
bindsym $mod+Ctrl+3 move container to workspace $ws3
bindsym $mod+Ctrl+4 move container to workspace $ws4
bindsym $mod+Ctrl+5 move container to workspace $ws5
bindsym $mod+Ctrl+6 move container to workspace $ws6
bindsym $mod+Ctrl+7 move container to workspace $ws7
bindsym $mod+Ctrl+8 move container to workspace $ws8
bindsym $mod+Ctrl+9 move container to workspace $ws9
bindsym $mod+Ctrl+0 move container to workspace $ws10


#################################################################
#  move focused window to a workspace and keep the focus on it  #
#################################################################

bindsym $mod+Shift+1 move container to workspace $ws1; workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2; workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3; workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4; workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5; workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6; workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7; workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8; workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9; workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10; workspace $ws10


########################################
#      navigate between workspaces     #
########################################

# focuses the next workspace
bindsym $mod+Ctrl+Right workspace next

# focuses the previous workspace
bindsym $mod+Ctrl+Left workspace prev


###############################################
#  move active workspace to the other monitor #
###############################################

bindsym $mod+m move workspace to output right


###############################################
#                  lock screen                #
###############################################

bindsym $mod+Shift+x exec --no-startup-id i3exit lock, mode "default"


########################################
#                                      #
#           MONITOR MANAGEMENT         #
#                                      #
########################################

bindsym $mod+p exec --no-startup-id "$HOME/.scripts/anytime/manage-display &"


########################################
#                                      #
#                 STYLE                #
#                                      #
########################################

########################################
#                 font                 #
########################################

font xft:Roboto 12


########################################
#              border style            #
########################################

new_window pixel 5
new_float pixel 5
gaps inner 10
gaps outer 0
smart_gaps on
smart_borders on


########################################
#              theme color             #
########################################

# class                   border  backgr. text    indic.   child_border
client.focused          #556064 #556064 #80FFF9 #FDF6E3 #6ab7ff
client.focused_inactive #2F3D44 #2F3D44 #1ABC9C #454948 #268BD0
client.unfocused        #2F3D44 #2F3D44 #1ABC9C #454948 #268BD0
client.urgent           #CB4B16 #FDF6E3 #1ABC9C #268BD2 #b71c1c
client.placeholder      #000000 #0c0c0c #ffffff #000000

client.background       #2B2C2B


########################################
#                                      #
#         APPLICATIONS AUTOSTART       #
#                                      #
########################################

exec --no-startup-id "$HOME/.scripts/startup/start-from-i3 &"


########################################
#                                      #
#             SOUND SECTION            #
#                                      #
########################################

#increase sound volume
bindsym XF86AudioRaiseVolume exec amixer set Master 5%+

#decrease sound volume
bindsym XF86AudioLowerVolume exec amixer set Master 5%-

# mute sound
bindsym XF86AudioMute exec amixer -D pulse set Master 1+ toggle
