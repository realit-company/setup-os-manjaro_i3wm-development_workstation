#!/bin/sh

install_packages(){
  local packages_to_install=$@
  local DISTRO=$(uname -r)

  case $DISTRO in
    *"MANJARO"*)
      yay -Sy $packages_to_install --noconfirm --needed
      ;;
    *"ARCH"*)
      yay -Sy $packages_to_install --noconfirm --needed
      ;;
    *)
      echo "Sorry, your distro is not handled by our script."
  esac
}

install_not_installed_packages(){
  local packages_wanted=$@
  local packages_to_install=""

  for package in $packages_wanted
  do
    if !(yay -Q $package >/dev/null); then
      echo "$package is not installed"
      packages_to_install+="$package "
    fi
  done

  install_packages "$packages_to_install"
}

update_all_package_repositories() {
  local DISTRO=$(uname -r)

  case $DISTRO in
    *"MANJARO"*)
      sudo pacman -Sy yay --noconfirm --needed
      trizen -Sy --noconfirm
      ;;
    *"ARCH"*)
      sudo pacman -Sy yay --noconfirm --needed
      yay -Sy --noconfirm
      ;;
    *)
      echo "Sorry, your distro is not handled by our script."
  esac
}


backup_and_remove_directory(){
  local directory_path=$1
  local directory_backup_path=$2

  if [ -d $directory_path ]; then
    if [ ! -d $directory_backup_path ]; then
      mv $directory_path $directory_backup_path
    else
      rm -rf $directory_backup_path
      mv $directory_path $directory_backup_path
    fi
  fi
}

backup_and_remove_file(){
  local file_path=$1
  local file_backup_path=$2

  if [ -f $file_path ]; then
    if [ ! -f $file_backup_path ]; then
      mv $file_path $file_backup_path
    else
      rm $file_backup_path
      mv $file_path $file_backup_path
    fi
  fi
}

default_var(){
  local var=$1
  local default_var=$2
  
  local var="$(grep -oP '(?<=#).*?(?=#)' <<< "$var")"

  if [ -z "$var" ]; then
    echo $default_var
  else
    echo $var
  fi
}

get_monitor_width(){
  echo "$(grep -oP '(?<=^).*?(?=x)' <<< $(xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/'))"
}

get_monitor_heigth()(
  echo "$(grep -oP '(?<=x).*?(?=$)' <<< $(xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/'))"
)