#!/bin/sh

setup_polybar(){
  local PATH_DIR_POLYBAR=$HOME/.config/polybar
  local PATH_DIR_POLYBAR_BAK=$HOME/.config/polybar.bak
  
  backup_and_remove_directory $PATH_DIR_POLYBAR $PATH_DIR_POLYBAR_BAK
  mkdir -p $PATH_DIR_POLYBAR
  cp -r $PATH_INSTALLER_ROOT/configuration/polybar/* $PATH_DIR_POLYBAR
}