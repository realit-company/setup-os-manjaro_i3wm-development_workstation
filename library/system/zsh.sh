#!/bin/sh

setup_zsh(){
  local PATH_DIR_ZSH_CONFIG=$HOME/.zsh
  local PATH_DIR_ZSH_CONFIG_BAK=$HOME/.zsh.bak

  install_not_installed_packages "
    zsh
    figlet 
    lsd
  "

  backup_and_remove_directory $PATH_DIR_ZSH_CONFIG $PATH_DIR_ZSH_CONFIG_BAK

  mkdir $PATH_DIR_ZSH_CONFIG
  
  cp $PATH_INSTALLER_ROOT/configuration/zsh/*.zsh $PATH_DIR_ZSH_CONFIG

  cp $PATH_INSTALLER_ROOT/configuration/zsh/.zshrc $HOME

  usermod --shell /bin/zsh $USER
}