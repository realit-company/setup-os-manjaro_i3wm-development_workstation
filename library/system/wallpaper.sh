#!/bin/sh

setup_wallpaper(){

  local PATH_DIR_WALLPAPER=$HOME/.config/wallpaper
  local PATH_DIR_WALLPAPER_BAK=$HOME/.config/wallpaper.bak
  
  backup_and_remove_directory $PATH_DIR_WALLPAPER $PATH_DIR_WALLPAPER_BAK
  mkdir -p $PATH_DIR_WALLPAPER
  cp --force $PATH_INSTALLER_ROOT/configuration/wallpaper/* $PATH_DIR_WALLPAPER
}