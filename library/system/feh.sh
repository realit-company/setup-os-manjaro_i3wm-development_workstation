#!/bin/sh

setup_feh(){
  local PATH_FILE_FEH=$HOME/.fehbg
  local PATH_FILE_FEH_BAK=$HOME/.fehbg.bak
  
  backup_and_remove_file $PATH_FILE_FEH $PATH_FILE_FEH_BAK
  cp --force $PATH_INSTALLER_ROOT/configuration/feh/.fehbg $PATH_FILE_FEH
}