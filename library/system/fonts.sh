#!/bin/sh

setup_fonts(){
  mkdir -p $HOME/.fonts
  cp --force $PATH_INSTALLER_ROOT/configuration/fonts/* $HOME/.fonts
}
