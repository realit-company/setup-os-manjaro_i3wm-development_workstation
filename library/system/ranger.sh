#!/bin/sh

setup_ranger(){
  local PATH_DIR_RANGER=$HOME/.config/ranger
  local PATH_DIR_RANGER_BAK=$HOME/.config/ranger.bak

  install_not_installed_packages "
    dragon
  "
    
  backup_and_remove_directory $PATH_DIR_RANGER $PATH_DIR_RANGER_BAK
  mkdir -p $PATH_DIR_RANGER
  cp -r $PATH_INSTALLER_ROOT/configuration/ranger/* $PATH_DIR_RANGER
}