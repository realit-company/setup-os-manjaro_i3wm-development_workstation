#!/bin/sh

setup_termite(){
  local PATH_DIR_TERMITE_CONFIG=$HOME/.config/termite
  local PATH_DIR_TERMITE_CONFIG_BAK=$HOME/.config/termite.bak

  install_not_installed_packages "
    termite
  "
    
  backup_and_remove_file $PATH_DIR_TERMITE_CONFIG $PATH_DIR_TERMITE_CONFIG_BAK
  mkdir -p $PATH_DIR_TERMITE_CONFIG
  cp $PATH_INSTALLER_ROOT/configuration/termite/* $PATH_DIR_TERMITE_CONFIG
}

