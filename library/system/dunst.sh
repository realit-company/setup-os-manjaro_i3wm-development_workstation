#!/bin/sh

setup_dunst(){
  local PATH_DIR_DUNST=$HOME/.config/dunst
  local PATH_DIR_DUNST_BAK=$HOME/.config/dunst.bak

  backup_and_remove_directory $PATH_DIR_DUNST $PATH_DIR_DUNST_BAK
  mkdir -p $PATH_DIR_DUNST
  cp -r $PATH_INSTALLER_ROOT/configuration/dunst/* $PATH_DIR_DUNST
}