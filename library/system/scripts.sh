#!/bin/sh

setup_scripts(){
  local PATH_DIR_SCRIPTS=$HOME/.scripts
  local PATH_DIR_SCRIPTS_BAK=$HOME/.scripts.bak

  backup_and_remove_directory $PATH_DIR_SCRIPTS $PATH_DIR_SCRIPTS_BAK
  mkdir -p $PATH_DIR_SCRIPTS
  cp -r $PATH_INSTALLER_ROOT/configuration/scripts/* $PATH_DIR_SCRIPTS
}
