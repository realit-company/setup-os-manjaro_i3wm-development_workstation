#!/bin/sh

setup_compton(){
  local PATH_DIR_COMPTON=$HOME/.config/compton
  local PATH_DIR_COMPTON_BAK=$HOME/.config/compton.bak
  
  install_not_installed_packages "
    compton
  "

  backup_and_remove_directory $PATH_DIR_COMPTON $PATH_DIR_COMPTON_BAK
  mkdir -p $PATH_DIR_COMPTON
  cp -r $PATH_INSTALLER_ROOT/configuration/compton/* $PATH_DIR_COMPTON
}