#!/bin/sh

setup_vim(){

  local PATH_DIR_VIM_CONFIG=$HOME/.vim
  local PATH_DIR_VIM_CONFIG_BAK=$HOME/.vim.bak

  install_not_installed_packages "
      vim
  "

  backup_and_remove_directory $PATH_DIR_VIM_CONFIG $PATH_DIR_VIM_CONFIG_BAK
  mkdir $PATH_DIR_VIM_CONFIG
  mkdir -p $PATH_DIR_VIM_CONFIG/config
  mkdir -p $PATH_DIR_VIM_CONFIG/pack/$USER/start
  mkdir -p $PATH_DIR_VIM_CONFIG/after/ftplugin

  cp $PATH_INSTALLER_ROOT/configuration/vim/ftplugin/* $PATH_DIR_VIM_CONFIG/after/ftplugin/
}

setup_vim_cctree(){
  local path_vim_plugin=$HOME/.vim/pack/$USER/start/cctree
  local git_repository="https://github.com/hari-rangarajan/CCTree"

  if [ ! -d $path_vim_plugin ]; then
    git clone $git_repository $path_vim_plugin
  fi
}

setup_vim_colorized(){
  local path_vim_plugin=$HOME/.vim/pack/$USER/start/colorizer
  local git_repository="https://github.com/vim-scripts/colorizer"
  
  if [ ! -d $path_vim_plugin ]; then
    git clone $git_repository $path_vim_plugin
  fi
}

setup_vim_doxygentoolkit(){
  local path_vim_plugin=$HOME/.vim/pack/$USER/start/DoxygenToolkit
  local git_repository="https://github.com/vim-scripts/DoxygenToolkit.vim"

  if [ ! -d $path_vim_plugin ]; then
    git clone $git_repository $path_vim_plugin
  fi
}

# setup_vim_easytags(){
  # local path_vim_plugin=$HOME/.vim/pack/$USER/start/easytag
  # local git_repository="https://github.com/xolox/vim-easytags"

  # if [ ! -d $path_vim_plugin ]; then
  #   cp --force $PATH_INSTALLER_ROOT/configuration/vim/94-easytag.vim $HOME/.vim/config/
  #   git clone $git_repository $path_vim_plugin
  # fi
# }

setup_vim_fugitive(){
  local path_vim_plugin=$HOME/.vim/pack/$USER/start/fugitive
  local git_repository="https://github.com/tpope/vim-fugitive"

  if [ ! -d $path_vim_plugin ]; then
    git clone $git_repository $path_vim_plugin
  fi
}

setup_vim_lightline(){
  local path_vim_plugin=$HOME/.vim/pack/$USER/start/lightline
  local git_repository="https://github.com/itchyny/lightline.vim"

  if [ ! -d $path_vim_plugin ]; then
    git clone $git_repository $path_vim_plugin
  fi 
}

setup_vim_minibuf(){
  local path_vim_plugin=$HOME/.vim/pack/$USER/start/minibuf
  local git_repository="https://github.com/fholgado/minibufexpl.vim"

  if [ ! -d $path_vim_plugin ]; then
    cp $PATH_INSTALLER_ROOT/configuration/vim/95-minibuf.vim $HOME/.vim/config/
    git clone $git_repository $path_vim_plugin
  fi
}

setup_vim_misc(){
  local path_vim_plugin=$HOME/.vim/pack/$USER/start/misc
  local git_repository="https://github.com/xolox/vim-misc"

  if [ ! -d $path_vim_plugin ]; then
    git clone $git_repository $path_vim_plugin
  fi
}

setup_vim_nerdtree(){
  local path_vim_plugin=$HOME/.vim/pack/$USER/start/nerdtree
  local git_repository="https://github.com/scrooloose/nerdtree"
  
  git clone  

  if [ ! -d $path_vim_plugin ]; then
    cp $PATH_INSTALLER_ROOT/configuration/vim/92-nerdtree.vim $HOME/.vim/config/
    git clone $git_repository $path_vim_plugin
  fi
}

# TODO
setup_vim_qfgrep(){

  cp $PATH_INSTALLER_ROOT/configuration/vim/96-qfgrep.vim $HOME/.vim/config/  
  git clone $git_repository $path_vim_plugin
  git clone https://github.com/yegappan/grep.git $HOME/.vim/pack/$USER/start/grep
  git clone https://github.com/sk1418/QFGrep.git $HOME/.vim/pack/$USER/start/QFGrep
}

# TODO
setup_vim_syntastic(){
  cp $PATH_INSTALLER_ROOT/configuration/vim/97-syntastic.vim $HOME/.vim/config/
  git clone https://github.com/vim-syntastic/syntastic.git $HOME/.vim/pack/$USER/start/syntastic
}


# TODO
setup_vim_tagbar(){
  cp $PATH_INSTALLER_ROOT/configuration/vim/93-tagbar.vim $HOME/.vim/config/
  git clone https://github.com/majutsushi/tagbar $HOME/.vim/pack/$USER/start/tagbar
}

setup_vim_ultisnips(){
  cp $PATH_INSTALLER_ROOT/configuration/vim/98-ultisnips.vim $HOME/.vim/config/
  git clone https://github.com/sirver/UltiSnips $HOME/.vim/pack/$USER/start/UtilSnips
  git clone https://github.com/honza/vim-snippets $HOME/.vim/pack/$USER/start/vim-snippets
}

setup_vim_vanilla(){
  cp $PATH_INSTALLER_ROOT/configuration/vim/0*.vim $PATH_INSTALLER_ROOT/configuration/vim/1*.vim $PATH_INSTALLER_ROOT/configuration/vim/2*.vim $HOME/.vim/config/
  cp $PATH_INSTALLER_ROOT/configuration/vim/.vimrc $HOME
}

setup_vim_youcompleteme(){
  cp $PATH_INSTALLER_ROOT/configuration/vim/91-ycm.vim $HOME/.vim/config/
  git clone https://github.com/rdnetto/YCM-Generator $HOME/.vim/pack/$USER/start/YCM-Generator
  git clone https://github.com/Valloric/YouCompleteMe $HOME/.vim/pack/$USER/start/YouCompleteMe
  cd  $HOME/.vim/pack/$USER/start/YouCompleteMe
  git submodule update --init --recursive
  ./install.py --clang-completer --java-completer
  cd -
}

setup_vim_plugins(){
  local vim_plugins_to_install=$@

  for plugin in $vim_plugins_to_install
  do
    $("setup_vim_$plugin")
  done
}