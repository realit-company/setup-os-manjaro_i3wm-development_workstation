#!/bin/sh

setup_conky(){
  local PATH_DIR_CONKY=$HOME/.config/conky
  local PATH_DIR_CONKY_BAK=$HOME/.config/conky.bak

  backup_and_remove_directory $PATH_DIR_CONKY $PATH_DIR_CONKY_BAK
  mkdir -p $PATH_DIR_CONKY
  cp -r $PATH_INSTALLER_ROOT/configuration/conky/* $PATH_DIR_CONKY
}