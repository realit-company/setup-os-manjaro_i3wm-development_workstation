#!/bin/sh

setup_i3(){
  local PATH_DIR_I3_CONFIG=$HOME/.i3
  local PATH_DIR_I3_CONFIG_BAK=$HOME/.i3.bak

  local PATH_FILE_MIMEAPPS=$HOME/.config/mimeapps.list
  local PATH_FILE_MIMEAPPS_BAK=$HOME/.config/mimeapps.list.bak

  local PATH_FILE_XRESOURCES=$HOME/.Xresources
  local PATH_FILE_XRESOURCES_BAK=$HOME/.Xresources.bak

  local PATH_FILE_PROFILE=$HOME/.profile
  local PATH_FILE_PROFILE_BAK=$HOME/.profile.bak


  # install packages
  install_not_installed_packages "
    i3-gaps \
    git \
    xorg-xbacklight \
    feh \
    ranger \
    termite \
    polybar \
    rofi \
    flameshot \
    arandr \
    blueman \
    rfkill \
    fprintd \
    imagemagick
  "

  # install i3 config
  backup_and_remove_directory $PATH_DIR_I3_CONFIG $PATH_DIR_I3_CONFIG_BAK
  mkdir -p $PATH_DIR_I3_CONFIG
  cp -r $PATH_INSTALLER_ROOT/configuration/i3/* $PATH_DIR_I3_CONFIG

  # install mimeapps_file
  backup_and_remove_file $PATH_FILE_MIMEAPPS $PATH_FILE_MIMEAPPS_BAK
  cp $PATH_INSTALLER_ROOT/configuration/mimeapps.list $PATH_FILE_MIMEAPPS

  # install Xresources file
  backup_and_remove_file $PATH_FILE_XRESOURCES $PATH_FILE_XRESOURCES_BAK
  cp $PATH_INSTALLER_ROOT/configuration/.Xresources $PATH_FILE_XRESOURCES

  # install profile file
  backup_and_remove_file $PATH_FILE_PROFILE $PATH_FILE_PROFILE_BAK
  cp $PATH_INSTALLER_ROOT/configuration/.profile $PATH_FILE_PROFILE

  # install bluetooth
  sudo systemctl enable bluetooth.service
  sudo systemctl start bluetooth.service

  # disable acpi power button event
  sudo sed -i "s/.*HandlePowerKey=.*/HandlePowerKey=ignore/g" /etc/systemd/logind.conf

  # restart
  xrdb $PATH_FILE_XRESOURCES
  i3-msg restart

  

  # install fingerprint sensor login
  # if ! grep -q "auth      sufficient pam_fprintd.so" "/etc/pam.d/system-local-login"; then
  #   sudo sed -i '3iauth      sufficient pam_fprintd.so' /etc/pam.d/system-local-login
  # fi
}


