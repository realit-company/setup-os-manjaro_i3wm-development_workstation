#!/bin/sh

setup_archlinux_xorg(){
  install_packages "
    xorg-server \
    xorg-apps \
    xorg-xinit \
    xf86-video-intel \
    mesa \
    xf86-input-libinput
  "
}

setup_archlinux_i3(){
  install_packages "
    i3-gaps
  "
}

setup_archlinux_network(){
  install_packages "
    wpa_supplicant \
    dialog \
    networkmanager \
    openvpn \
    networkmanager-openvpn \
    networkmanager-vpnc \
    network-manager-applet \
    dhclient \
    libsecret
  "
}

setup_archlinux_bluetooth(){
  install_packages "
    bluez \
    bluez-utils \
    bluez-firmware \
    blueberry \
    pulseaudio-bluetooth \
    bluez-libs
  "
}

setup_archlinux_audio(){
   install_packages "
    alsa-utils \
    alsa-plugins \
    pulseaudio \
    pulseaudio-alsa
  "
}

setup_archlinux_printers(){
  install_packages "
    cups \
    cups-pdf \
    ghostscript \
    gsfonts \
    hplip \
    system-config-printer
  "
}

setup_archlinux_utilities(){

  # terminal
  install_packages "
    curl \
    gnome-keyring \
    gtop \
    hardinfo \
    htop \
    inxi \
    jq \
    neofetch \
    ntp \
    numlockx \
    openssh \
    rsync \
    speedtest-cli \
    tlp \
    unrar \
    unzip \
    wget \
    zip \
    zsh \
    zsh-completions
  "
  
  # disk
  install_packages "
    autofs \
    exfat-utils \
    gparted \
    gnome-disks \
    ntfs-3g \
    parted
  "

  # softwares
  install_packages "
    filezilla \
    flashplugin \
    vlc
  "

  #productivity
  install_packages "
    filezilla \
    flashplugin \
    hunspell \
    hunspell-en \
    hunspell-fr \
    libreoffice-fresh \
    vlc
  "

  # virtualization
  install_packages "
    virtualbox \
    virtualbox-host-modules-arch
  "

  # system
  install_packages "
    gtkhash
  "
}

setup_archlinux_final(){
  # xinitc
  touch $HOME/.xinitrc
  echo '#!/bin/bash
    if [ -d /etc/X11/xinit/xinitrc.d ] ; then
        for f in /etc/X11/xinit/xinitrc.d/?*.sh ; do
            [ -x "\$f" ] && . "\$f"
        done
        unset f
    fi
    source /etc/xdg/xfce4/xinitrc
    exit 0' >> $HOME/.xinitrc
  
  # By default, startx incorrectly looks for the .serverauth file in our HOME folder.
  sudo sed -i 's|xserverauthfile=\$HOME/.serverauth.\$\$|xserverauthfile=\$XAUTHORITY|g' /bin/startx

  # configure lts kernel
  sudo cp /boot/loader/entries/arch.conf /boot/loader/entries/arch-lts.conf
  sudo sed -i 's|Arch Linux|Arch Linux LTS Kernel|g' /boot/loader/entries/arch-lts.conf
  sudo sed -i 's|vmlinuz-linux|vmlinuz-linux-lts|g' /boot/loader/entries/arch-lts.conf
  sudo sed -i 's|initramfs-linux.img|initramfs-linux-lts.img|g' /boot/loader/entries/arch-lts.conf

  # makepkg with all cores
  sudo sed -i -e 's|[#]*MAKEFLAGS=.*|MAKEFLAGS="-j$(nproc)"|g' makepkg.conf
  sudo sed -i -e 's|[#]*COMPRESSXZ=.*|COMPRESSXZ=(xz -c -T 8 -z -)|g' makepkg.conf


  # configure login shell
  echo 'KEYMAP=us
  FONT=ter-v32b' >> /etc/vconsole.conf


  # laptop lid close = suspend
  sudo sed -i -e 's|[# ]*HandleLidSwitch[ ]*=[ ]*.*|HandleLidSwitch=suspend|g' /etc/systemd/logind.conf
  

  # fix cursor with multiple monitors
  echo '[Icon Theme]
  #Inherits=Theme' >> /usr/share/icons/default/index.theme


  # increase filewatcher count
  echo fs.inotify.max_user_watches=524288 | sudo tee /etc/sysctl.d/40-max-user-watches.conf && sudo sysctl --system


  # enable bluetooth daemon
  sudo sed -i 's|#AutoEnable=false|AutoEnable=true|g' /etc/bluetooth/main.conf
  sudo systemctl enable bluetooth.service
  sudo systemctl start bluetooth.service

  # enable cups for printing
  systemctl enable org.cups.cupsd.service
  systemctl start org.cups.cupsd.service

  # enable network time
  sudo ntpd -qg
  sudo systemctl enable ntpd.service
  sudo systemctl start ntpd.service

  # network config
  read -p "ENTER YOUR IP LINK: " LINK
  echo "Disabling DHCP and enabling Network Manager daemon"

  sudo systemctl disable dhcpcd.service
  sudo systemctl stop dhcpcd.service
  sudo ip link set dev ${LINK} down
  sudo systemctl enable NetworkManager.service
  sudo systemctl start NetworkManager.service
  sudo ip link set dev ${LINK} up
}