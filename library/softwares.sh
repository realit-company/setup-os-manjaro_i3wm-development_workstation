#!/bin/sh

install_not_installed_packages "
    jetbrains-toolbox \
    gitkraken \
    google-chrome \
    typora \
    vscodium-bin \
    pavucontrol \
    xfce4-power-manager \
    xfce4-settings-gtk3 \
    clipit \
    arandr \
    blueman \
    bluez \
    bluez-tools \
    rfkill \
    insync \
    discord \
    electron \
    postman \
    imagemagick \
    fingerprint-gui
  "