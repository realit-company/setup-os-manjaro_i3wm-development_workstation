#!/bin/sh

setup_python_development_environment(){
  install_not_installed_packages "
      python \
      python-pip \
      python-virtualenvwrapper
  "
  
  cp --force $PATH_INSTALLER_ROOT/configuration/python/python.zsh $HOME/.zsh/
}
