#!/bin/sh

setup_low_level_development_environment(){
  install_not_installed_packages "
      gcc \
      cmake \
      radare2 \
      radare2-cutter
  "
}