#!/bin/sh

setup_java_development_environment(){

  install_not_installed_packages "
      jdk8-openjdk \
      jre8-openjdk \
      java8-openjfx \
      gluon-scenebuilder
  "

  install_packages $PACKAGES_TO_INSTALL
  
  sudo archlinux-java set java-8-openjdk
}