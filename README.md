# os-manjaro-i3wm-post-install-script

This project aim is to automate the configuration of an archlinux-based operating system.

## Usage

You have the choice to use :

1. A manual configuration
2. An automatic default configuration
3. Automatic configurations by profile


### Manual configuration

Just execute the intaller-dialog shell script and select "manual installation".
> Then select the options you would like.

### Automatic default configuration

Just execute the intaller-dialog shell script and select "default"


### Automatic configuration by profile

1. Create / modify a file named with your choosen profile name
2. Add / remove function calls to process or not the installations you desire


## TODO

audio control: alsamixer
image viewer: feh
pdf viewer: mupdf

